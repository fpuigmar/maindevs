package org.entreculturas.test;

import org.entreculturas.entities.Socio;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SocioTest {
    private Socio Socio;

    @Test
    public void Socio_datos(){
        Integer id= 3;
        String nombre="Carlos";
        String DNI="123456";
        String TipoCuota="Mensual";
        Integer idProyecto=1;

        Socio = new Socio( nombre, DNI,id,TipoCuota,idProyecto);

        Assert.assertEquals(id, this.Socio.getIdSocio());
        Assert.assertEquals(nombre, this.Socio.getNombre());
        Assert.assertEquals(DNI, this.Socio.getDNI());
        Assert.assertEquals(TipoCuota, this.Socio.getTipoCuota());
        Assert.assertEquals(idProyecto, this.Socio.getProyectoId());
    }

}