package org.entreculturas.test;

import org.entreculturas.entities.Delegacion;
import org.junit.Assert;
import org.junit.Test;

public class DelegacionTest {
    private Delegacion Delegacion;

    @Test
    public void Delegaciones_datos(){
        String nombre = "Pepe";
        String direccion = "Calle Villaroel";
        String telefono = "617679548";
        String email = "tets@test.com";

        Delegacion = new Delegacion(null, nombre, direccion, telefono, email);
        Assert.assertEquals(nombre, this.Delegacion.getNombre());
        Assert.assertEquals(direccion, this.Delegacion.getDireccion());
        Assert.assertEquals(telefono, this.Delegacion.getTelefono());
        Assert.assertEquals(email, this.Delegacion.getEmail());
    }
}