package org.entreculturas.test;

import org.entreculturas.entities.LineaAccion;
import org.entreculturas.entities.Pais;
import org.entreculturas.entities.Proyecto;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class ProyectoTest {
    private Proyecto proyecto;

    @Test
    public void Proyecto_datos(){
        Integer id = 2;
        String nombre = "Maria";
        Pais sevilla = new Pais(null, "Sevilla");
        LineaAccion accion = new LineaAccion(
            null,
            "Cooperación al desarrollo",
            null,
            2);

        String fechaFin = "20-07-2025";
        String fechaInicio = "20-08-2021";
        String financiador = "La CaixaBank";
        int financiacion = 1000000;

        proyecto = new Proyecto(
                null,
                nombre,
                fechaInicio,
                fechaFin,
                financiador,
                financiacion,
                accion.getId(),
                20,
                sevilla.getId()
                );

        Assert.assertEquals(id, this.proyecto.getIdProyecto());
        Assert.assertEquals(nombre, this.proyecto.getNombre());
//        Assert.assertEquals(sevilla.getNombre(), this.proyecto.getLocalizacion());
        Assert.assertEquals(fechaInicio, this.proyecto.getFechaInicio());
        Assert.assertEquals(fechaFin, this.proyecto.getFechafin());
        Assert.assertEquals(fechaFin, this.proyecto.getFechafin());
        Assert.assertEquals(financiador, this.proyecto.getFinanciador());
        Assert.assertEquals(Integer.toString(financiacion), this.proyecto.getFinanciacion());

    }


}