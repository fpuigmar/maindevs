package org.entreculturas.test;

import org.entreculturas.entities.Pais;
import org.junit.Assert;
import org.junit.Test;

public class PaisTest {
    private Pais Pais;

    @Test
    public void Pais_datos(){
        Integer id = 1;
        String nombre = "España";
        Pais = new Pais(id, nombre);

        Assert.assertEquals(id, this.Pais.getId());
        Assert.assertEquals(nombre, this.Pais.getNombre());
    }
}